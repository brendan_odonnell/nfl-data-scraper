from selenium import webdriver
from selenium.webdriver.common.by import By
from getCurrentWeek import *
from time import sleep
import json

options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument('--ignore-ssl-errors')
options.add_argument("--test-type")
#options.binary_location = "/usr/bin/chromium"

def clickButton():
    driver = webdriver.Chrome(chrome_options=options)
    driver.get('https://www.pro-football-reference.com/years/2020')
    sleep(5)
    html = driver.execute_script("return document.getElementsByTagName('html')[0].innerHTML")
    # click radio button
    python_button = driver.find_element_by_id('team_stats_per_match_toggle')
    #print(python_button)
    python_button.click()
    rankings = driver.find_elements(By.CSS_SELECTOR, "th[data-stat='ranker']")
    d = {}
    for rank in rankings:
        r = rank.text
        if r == 'Rk':
            continue
        if not r.isspace():
            d[r] = []

    stat = driver.find_elements(By.CSS_SELECTOR, "td[data-original-value]")
    index = '1'
    for i,item in enumerate(stat):
        j = i + 1
        d[index].append(item.text)
        if not j % 20 and j != 0:
            index = str(1+int(index))
            if int(index) > 32:
                break
    return d

'''
with open('updatedShit.json', 'w') as f:
    f.write(json.dumps(d, indent=4))

with open('updatedShit.json', 'r') as f:
    d = json.load(f)
'''
def updateWeekly(standings, d, gameNum, write=False):
    hackyShite = {0:'PF', 1:'Yds', 2:'Ply', 3:'TO', 4:'FL', 5:'1stD', 6:'Cmp', 7:'Att', 8:'Yds', 9:'TD', 10:'Int', 11:'1stD',
    12:'Att', 13:'Yds', 14:'TD', 15:'1stD', 16:'Pen', 17:'Yds', 18:'1stPy', 19:'EXP'}

    for conference in standings.keys():
        for division in standings[conference].keys():
            for team in standings[conference][division].keys():
                currentRank = standings[conference][division][team]['game'][gameNum]['Total']['Rk']
                for i, stat in enumerate(d[currentRank]):
                    statString = hackyShite[i]
                    if i < 6 or i == 19:
                        standings[conference][division][team]['game'][gameNum]['Total'][statString] = stat
                    if i >= 6 and i < 12:
                        standings[conference][division][team]['game'][gameNum]['Passing'][statString] = stat
                    if i >= 12 and i < 16:
                        standings[conference][division][team]['game'][gameNum]['Rushing'][statString] = stat
                    if i >= 16 and i < 19:
                        standings[conference][division][team]['game'][gameNum]['Penalties'][statString] = stat

    with open('overallStandings.json', 'w') as f:
        f.write(json.dumps(standings, indent=4))

    return standings

def main():
    write = True

    respString = sendReq()

    weeklyStats, weekNum = getWeeklyStats(respString, write)

    updatedStats = updateStandings(weeklyStats, weekNum, write)

    perGameStats = clickButton()

    updatedWeeklyStats = updateWeekly(updatedStats, perGameStats, weekNum, write)

main()
