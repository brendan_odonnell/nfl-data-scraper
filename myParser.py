from html.parser import HTMLParser


class findTable(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.extract = 0
        self.data = []

    def handle_starttag(self, tag, attrs):
        if tag == 'table':
            self.extract = 1

    def handle_endtag(self, tag):
        if tag == 'table':
            self.extract = 0

    def handle_data(self, data):
        if self.extract and not data.isspace():
            self.data.append(data)
