import requests as req
import json

from myParser import findTable

websites = ['https://www.pro-football-reference.com/years/2020/']

header = {'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'}

#for site in websites:
resp = req.request(method='GET', headers=header, url=websites[0])

respString = str(resp.content).replace('\n','')
respString = respString.replace('\t', '')
respString = respString.replace('\\n', '')

parse = findTable()
parse.feed(respString)
respString = parse.data

### Print ###
'''
i = 0
for data in respString:
	i += 1
	print(data, end=' ')
	if 'AFC' in data or 'NFC' in data:
		print()
		i -= 1
		continue
	if not i % tableLen:
		print()
'''
#############

### AFC & NFC ###
standings = {}
tableLen = 13
divisionLen = tableLen * 4

while(respString):

	index = 0
	conference = respString.pop(index)[:3]
	standings[conference] = {}
	for i in range(4):
		division = respString.pop(tableLen+divisionLen*i).strip()
		standings[conference][division] = {}

		for j in range(4):
			index = divisionLen*i+tableLen+tableLen*j
			team = respString[index]
			standings[conference][division][team] = {}

			for k, stat in enumerate(respString[1:tableLen]):
				index = tableLen+divisionLen*i+tableLen*j+k+1
			#	standings[conference][division][team][stat] = respString[index]

	respString = respString[index+1:]


### Write to json ###
#print(json.dumps(standings, indent=4))
with open('standings.json', 'w') as f:
	f.write(json.dumps(standings, indent=4))
#####################
