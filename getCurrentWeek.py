import requests as req
import json

from parseStats import findStats
from myParser import findTable

websites = ['https://www.pro-football-reference.com/years/2020']

header = {'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'}




##############################################
def sendReq():
	resp = req.request(method='GET', headers=header, url=websites[0])

	resp = str(resp.content)
	resp = resp.replace('\t', '')
	resp = resp.replace('\\n', '')
	return resp
###############################################

###############################################
def getWeeklyStats(respString, write=False):
	## Parse for comments
	parse = findStats()
	parse.feed(respString)

	## Parse comments for tables
	table = findTable()
	table.feed(parse.data[32])
	weeklyStats = table.data[5:]

	### Get Cumulative stats by week ###
	tableLen = 28
	gameNum = weeklyStats[tableLen+2]
	statistics = weeklyStats[0:tableLen]
	cumulativeStats = {}

	for i in range(1,33):
		team = weeklyStats[i*tableLen+1]
		cumulativeStats[team] = {'Total':{}, 'Passing':{}, 'Rushing':{}, 'Penalties':{}}
		for j,stat in enumerate(statistics):
			index = i*tableLen+j
			if j == 0:
				cumulativeStats[team]['Total'][stat] = weeklyStats[index]
			if j <= 2:
				continue
			if j < 10 or j >= 25:
				cumulativeStats[team]['Total'][stat] = weeklyStats[index]
			if j >= 10 and j < 17:
				cumulativeStats[team]['Passing'][stat] = weeklyStats[index]
			if j >= 17 and j < 22:
				cumulativeStats[team]['Rushing'][stat] = weeklyStats[index]
			if j >= 22 and j < 25:
				cumulativeStats[team]['Penalties'][stat] = weeklyStats[index]

	if write:
		file = 'week{}'.format(gameNum)
		writeJson(file, cumulativeStats)

	return (cumulativeStats, gameNum)
###############################################

###############################################
def updateStandings(newWeekStats, gameNum, write=False):

	### Update Standings.json with cumulative stats ###
	with open('standings.json', 'r') as f:
		standings = json.load(f)

	for conference in standings.keys():
		for division in standings[conference].keys():
			for i,team in enumerate(standings[conference][division].keys()):
				standings[conference][division][team]['game'] = {}
				standings[conference][division][team]['game'][gameNum] = {'Total':{},'Passing':{}, 'Rushing':{}, 'Penalties':{}}
				for category in newWeekStats[team].keys():
					for stat in newWeekStats[team][category].keys():
						standings[conference][division][team]['game'][gameNum][category][stat] = newWeekStats[team][category][stat]
	if write:
		writeJson('overallStandings', standings)

	return standings
###############################################


###############################################
def writeJson(filename, jsonObj):
	with open('{}.json'.format(filename), 'w') as f:
		f.write(json.dumps(jsonObj, indent=4))
###############################################


def main():
	write = True
	respString = sendReq()

	weeklyStats, weekNum = getWeeklyStats(respString, write)

	updatedStats = updateStandings(weeklyStats, weekNum, write)

	#print(updatedStats['AFC']['AFC East'])
	#print(json.dumps(updatedStats, indent=4))

main()
